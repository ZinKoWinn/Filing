package com.startinpoint.filing.excel.style;

import cn.hutool.poi.excel.ExcelWriter;
import org.apache.poi.ss.usermodel.*;

/**
 * @author by Zin Ko Winn
 */

public class StyleUtils {
    public static CellStyle getHeaderCellStyle(ExcelWriter writer) {
        CellStyle customHeaderStyle = writer.createCellStyle();
        customHeaderStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        customHeaderStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        customHeaderStyle.setAlignment(HorizontalAlignment.LEFT);
        customHeaderStyle.setBorderTop(BorderStyle.THIN);
        customHeaderStyle.setBorderRight(BorderStyle.THIN);
        customHeaderStyle.setBorderBottom(BorderStyle.THIN);
        customHeaderStyle.setBorderLeft(BorderStyle.THIN);
        return customHeaderStyle;
    }

    public static CellStyle getBorderCellStyle(ExcelWriter writer) {
        CellStyle borderCellStyle = writer.createCellStyle();
        borderCellStyle.setAlignment(HorizontalAlignment.LEFT);
        borderCellStyle.setBorderTop(BorderStyle.THIN);
        borderCellStyle.setBorderRight(BorderStyle.THIN);
        borderCellStyle.setBorderBottom(BorderStyle.THIN);
        borderCellStyle.setBorderLeft(BorderStyle.THIN);
        return borderCellStyle;
    }

    public static CellStyle getUnderLineCellStyle(ExcelWriter writer) {
        Font font = writer.createFont();
        font.setUnderline(Font.U_SINGLE);
        CellStyle underLinedStyle = writer.createCellStyle();
        underLinedStyle.setAlignment(HorizontalAlignment.LEFT);
        underLinedStyle.setFont(font);
        return underLinedStyle;
    }
}
