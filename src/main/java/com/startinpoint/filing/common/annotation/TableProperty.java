package com.startinpoint.filing.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface TableProperty {

    String title() default "";

    int mergeColumnCount() default 1;

    boolean isHasTotal() default false;

    int totalMergeCount() default 1;
}
