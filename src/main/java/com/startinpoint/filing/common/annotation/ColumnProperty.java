package com.startinpoint.filing.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author by Zin Ko Winn
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ColumnProperty {

    int width() default 8000;

    int height() default 70;
    String dateFormat() default "dd/MM/yyyy";
}
