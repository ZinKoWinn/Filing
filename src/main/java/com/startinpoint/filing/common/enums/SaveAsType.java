package com.startinpoint.filing.common.enums;

/**
 * @author by Zin Ko Winn
 */
public enum SaveAsType {
    EXCEL,
    CSV,
}
